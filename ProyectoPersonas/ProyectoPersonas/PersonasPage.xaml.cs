﻿using ProyectoPersonas.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoPersonas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PersonasPage : ContentPage
	{
		public PersonasPage ()
		{
            this.BindingContext = new PersonasViewModel();
            InitializeComponent();
		}
	}
}