﻿using ProyectoPersonas.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoPersonas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
    {
        public LoginPage ()
		{
            BindingContext = new LoginViewModel();
            InitializeComponent ();
		}
    }
}