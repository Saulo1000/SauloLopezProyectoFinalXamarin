﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ProyectoPersonas
{
	public partial class App : Application
	{

        public static List<Models.Persona> ListaPersonas { get; set; }
        public App ()
		{
			InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
