﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProyectoPersonas.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        #region Atributos
        private string usuario;
        private string clave;
        private bool isEnabled;
        #endregion

        #region Propiedades
        public string Usuario
        {
            get { return usuario; }
            set
            {
                if (usuario != value)
                {
                    usuario = value;
                    OnPropertyChanged(nameof(Usuario));
                }
            }
        }
        public string Clave
        {
            get { return clave; }
            set
            {
                if (clave != value)
                {
                    clave = value;
                    OnPropertyChanged(nameof(Clave));
                }
            }
        }

        public bool IsEnabledBtn
        {
            get { return isEnabled; }
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    OnPropertyChanged(nameof(IsEnabledBtn));
                }
            }
        }
        #endregion

        #region Constructores
        public LoginViewModel()
        {
            Usuario = "jperez";
            Clave = "123";

            IsEnabledBtn = true;
        }
        #endregion

        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        private async void Login()
        {
            #region Validar
            if (string.IsNullOrEmpty(Usuario))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar un usuario", "Aceptar");
                return;
            }
            if (string.IsNullOrEmpty(Clave))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Debe ingresar la Clave", "Aceptar");
                return;
            }
            #endregion

            

            try
            {
                IsEnabledBtn = false;

                //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                if (Usuario == "jperez" && Clave == "123")
                {
                    IsEnabledBtn = true;
                    await Application.Current.MainPage.Navigation.PushAsync(new PersonasPage());
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Alerta", "Usuario o Clave Incorrecta", "Aceptar");
                    Usuario = string.Empty;
                    IsEnabledBtn = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Error", string.Format("Ocurrió el error: {0}", ex.Message), "Aceptar");
                IsEnabledBtn = true;
                return;
            }
        }
        #endregion
    }
}
