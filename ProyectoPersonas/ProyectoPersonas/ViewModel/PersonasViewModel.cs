﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using ProyectoPersonas.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using Xamarin.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Linq;

namespace ProyectoPersonas.ViewModel
{
    public class PersonasViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<Persona> _personas;
        private bool isRefreshing;
        private string filter;
        #endregion

        #region Properties
        public ObservableCollection<Models.Persona> Personas
        {
            get { return _personas; }
            set
            {
                if (_personas != value)
                {
                    _personas = value;
                    OnPropertyChanged(nameof(Personas));
                }
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    OnPropertyChanged(nameof(IsRefreshing));
                }
            }
        }
        public string Filter
        {
            get { return this.filter; }
            set
            {
                if (filter != value)
                {
                    filter = value;
                    OnPropertyChanged(nameof(Filter));
                    Search();
                }
            }
        }
        #endregion

        #region Constructores
        public PersonasViewModel()
        {
            CargarPersonas();
        }

        private async void CargarPersonas()
        {
            /*
            IsRefreshing = true;

            var response = await GetList<Models.Persona>(
                "http://restcountries.eu",
                "/rest",
                "/v2/all");

            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "OK");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            App.ListaPersonas = (List<Models.Persona>)response.Result;
            Personas = new ObservableCollection<Models.Persona>(ToPersonaItemViewModel());
            this.IsRefreshing = false;
            */

            IsRefreshing = true;

            var response = await GetList<Models.Persona>(
                "http://190.181.40.253",
                "/XamarinService",
                "/api/all");

            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "OK");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            App.ListaPersonas = (List<Models.Persona>)response.Result;
            Personas = new ObservableCollection<Models.Persona>(ToPersonaItemViewModel());
            this.IsRefreshing = false;
        }
        #endregion

        #region Metodos

        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(CargarPersonas);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }


        #endregion

        #region Commands
        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                Personas = new ObservableCollection<Models.Persona>(ToPersonaItemViewModel());
            }
            else
            {
                Personas = new ObservableCollection<Models.Persona>(
                    this.ToPersonaItemViewModel().Where(
                        l => l.Nombre.ToLower().Contains(this.Filter.ToLower()) ||
                             l.Ocupacion.ToLower().Contains(this.Filter.ToLower())));
            }
        }

        private IEnumerable<Models.Persona> ToPersonaItemViewModel()
        {
            return App.ListaPersonas.Select(persona => new Models.Persona
            {
                Nombre = persona.Nombre,
                Id = persona.Id,
                Ocupacion = persona.Ocupacion,
                Imagen = persona.Imagen
            });
        }

        public async Task<Response> GetList<T>(
            string urlBase,
            string servicePrefix,
            string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Ok",
                    Result = list,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
        #endregion
    }
}
