﻿using ProyectoPersonas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoPersonas.ViewModel
{
    public class PersonaViewModel : BaseViewModel
    {
        private Persona _persona;
        public Persona PersonaObj
        {
            get { return _persona; }
            set
            {
                if (_persona != value)
                {
                    _persona = value;
                    OnPropertyChanged(nameof(PersonaObj));
                }
            }
        }

        public PersonaViewModel(Persona persona)
        {
            PersonaObj = persona;
        }
    }
}
