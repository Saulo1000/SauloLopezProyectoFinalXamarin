﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Windows.Input;
using Xamarin.Forms;
using ProyectoPersonas.Tabs;

namespace ProyectoPersonas.Models
{
    public class Persona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Ocupacion { get; set; }
        public string Imagen { get; set; }

        public string PathImagen
        {
            get
            {
                return "http://190.181.40.253/XamarinService" + Imagen.Replace("~", "");
            }
        }

        public ICommand SelectPersonaCommand
        {
            get
            {
                return new RelayCommand(SelectDetail);
            }
        }

        private async void SelectDetail()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new PersonaTabbedPage(this));

        }
    }
}
