﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProyectoPersonas.ViewModel;

namespace ProyectoPersonas.Tabs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersonaTabbedPage : TabbedPage
    {
        public PersonaTabbedPage(Models.Persona persona)
        {
            this.BindingContext = new PersonaViewModel(persona);
            InitializeComponent();
        }
    }
}